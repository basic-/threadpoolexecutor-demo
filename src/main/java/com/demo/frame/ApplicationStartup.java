package com.demo.frame;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        // 显示你的横幅
        System.out.println(
                "////////////////////////////////\n" +
                        "//                            //\n" +
                        "//       工程启动成功           //\n" +
                        "//                            //\n" +
                        "////////////////////////////////"
        );

        try {
            Environment env = event.getApplicationContext().getEnvironment();
            String ip = InetAddress.getLocalHost().getHostAddress();
            String port = env.getProperty("server.port");
            String contextPath = env.getProperty("server.servlet.context-path");
            if (contextPath == null) {
                contextPath = "";
            }

            System.out.println("Local: http://localhost:" + port + contextPath + "/");
            System.out.println("External: http://" + ip + ":" + port + contextPath + "/");

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
