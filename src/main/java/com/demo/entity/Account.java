package com.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.demo.base.entity.BaseEntity;
import lombok.Data;

@Data
@TableName("account")
public class Account extends BaseEntity {

    private static final long serialVersionUID = -3306120382564873486L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号
     */
    @TableField("`accountNo`")
    private String accountNo;

    public Account(Long id, String accountNo) {
        this.id = id;
        this.accountNo = accountNo;
    }
}
