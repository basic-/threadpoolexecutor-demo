package com.demo.controller;

import com.demo.base.controller.BaseController;
import com.demo.entity.Account;
import com.demo.mapper.AccountMapper;
import com.demo.service.AccountService;
import com.demo.service.AsyncGenerateAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/account")
public class AccountController extends BaseController {

    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AccountService accountService;
    @Autowired
    AsyncGenerateAccountService asyncGenerateAccountService;

    @GetMapping("hello1")
    public String hello() {
        return "Hello1";
    }

    @GetMapping("getAccountList")
    public List<Account> getAccountList() {
        return accountMapper.selectList(null);
    }

    /**
     * 多线程生成账号
     */
    @PostMapping("generateAccounts")
    public void generateAccounts() throws ExecutionException, InterruptedException {
        accountService.generateAndInsertAccounts();
    }

    /**
     * 单线程异步生成账号
     */

    @PostMapping("/generateAccountSingleThread")
    public String generateAccountSingleThread() {
        asyncGenerateAccountService.generateAccountAndSave();
        return "Account generation in progress...";
    }
}
